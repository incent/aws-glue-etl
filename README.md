# AWS Glue Dynamic ETL Script

This is an open sourced project that provides a script to dynamically run ETL from AWS RDS instances to AWS Redshift using AWS Glue.

Using this script you can dynamically run an ETL from AWS RDS databases and tables to AWS Redshift. 

This script assumes that you have set up AWS Glue Cataloging beforehand as this is a prerequisite.

## Notes

- You can change the job schedules and triggers to suit your needs. Note that more frequent large data costs more money.
- Due to the limitations of how Redshift works to update data, I've take a flat approach and just completely clear the last data and re-write everything from the original dataset.
- This means that rather than appending the data, it just cleans and updates
- You can certainly modify the script so that it appends rather than clean and updates. Comment out `dynamic_frame1 = DropNullFields.apply(frame = dynamic_frame1)`
- The destination database can be configured to S3, Athena or even another RDS DB. I just use Redshift as I've optimised the Redshift cluster for complex queries. 

